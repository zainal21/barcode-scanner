package com.muhamadzain.barcode_scanner.ui

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.muhamadzain.barcode_scanner.R
import com.muhamadzain.barcode_scanner.base.BaseActivity

@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : BaseActivity() {

    override fun getLayoutResource(): Int = R.layout.activity_scanner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}